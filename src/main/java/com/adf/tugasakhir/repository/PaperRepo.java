package com.adf.tugasakhir.repository;

import com.adf.tugasakhir.model.Paper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaperRepo extends JpaRepository<Paper, Long> {

    List<Paper> findAll();
}
