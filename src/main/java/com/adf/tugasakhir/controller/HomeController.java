package com.adf.tugasakhir.controller;

import java.sql.Date;

import javax.persistence.EntityNotFoundException;

import com.adf.tugasakhir.dataclass.PaperResult;
import com.adf.tugasakhir.model.Conference;
import com.adf.tugasakhir.repository.ConferenceRepo;
import com.adf.tugasakhir.service.ConferenceService;
import com.adf.tugasakhir.service.PaperFinderService;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * HomeController
 */
@Log
@Controller
public class HomeController {

    @Autowired
    PaperFinderService paperFinderService;

    @Autowired
    ConferenceService conferenceService;

    @Autowired
    ConferenceRepo conferenceRepo; // testing purposes for testdb only. delete later.

    @GetMapping("/")
    @PreAuthorize("isAuthenticated()")
    public String home(Model model) {
        log.info("display home page");
        model.addAttribute("conferenceList", conferenceService.getAllLatestConference());
        log.info("list of all conferences " + conferenceService.getAllLatestConference().toString());
        return "_home";
    }

    @GetMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceForm(Model model) {
        log.info("display add conference form page");
        model.addAttribute("conference", new Conference());
        return "add-conference";
    }

    @PostMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceSubmit(@ModelAttribute Conference newConference, BindingResult bindingResult,
            RedirectAttributes redAttr) {
        log.info("post to add conference controller");
        if (bindingResult.hasErrors()) {
            log.warning("submission contains error");
            redAttr.addFlashAttribute("error_msg", "Terdapat kesalahan pada form ada.");
            return "redirect:/goof";
        }
        conferenceRepo.save(newConference);
        log.info("saved new conference " + newConference.toString());
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceForm(@PathVariable("id") long id, RedirectAttributes redirectAttributes,
            Model model) {
        log.info("display conference update page");
        try {
            model.addAttribute("conference", conferenceService.getConferenceById(id));
            log.info("conference found: " + conferenceService.getConferenceById(id).toString());
        } catch (EntityNotFoundException e) {
            log.warning("conference not found from id " + id);
            redirectAttributes.addFlashAttribute("error_msg", "Conference not found.");
            return "redirect:/goof";
        }
        return "update-conference";
    }

    @PostMapping("/update-conference")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceSubmit(@ModelAttribute Conference conference, RedirectAttributes redirectAttributes) {
        log.info("post to update conference controller");
        try {
            conferenceService.updateConference(conference);
            log.info("update conference " + conference.toString());
        } catch (EntityNotFoundException e) {
            log.warning("conference not found");
            redirectAttributes.addFlashAttribute("error_msg", "The conference that you want to update doesn't exist.");
            return "redirect:/goof";
        }
        return "redirect:/";
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String deleteConference(@PathVariable("id") long id) {
        log.info("post to delete conference controller");
        conferenceService.removeConferenceById(id);
        log.info("existing conference removed by id " + id);
        return "redirect:/";
    }

    @GetMapping("/view/{id}")
    @PreAuthorize("isAuthenticated()")
    public String viewConference(@PathVariable("id") long id, Model model, RedirectAttributes redAttr) {
        log.info("display view conference page");
        try {
            Conference conference = conferenceService.getConferenceById(id);
            model.addAttribute("conference", conference);
            log.info("conference found: " + conference);
            PaperResult pResult = paperFinderService.findPaperFromConference(conference);
            log.info("conference contains paper " + pResult);
            if (!pResult.isError()) {
                log.warning("error obtaining paper from conference");
                model.addAttribute("paperList", pResult.getPaperList());
                log.info("conference contains paper " + pResult);
            }

            return "conference-detail";
        } catch (EntityNotFoundException e) {
            log.warning("conference not found from id " + id);
            redAttr.addFlashAttribute("error_msg", "Conference not found.");
            return "redirect:/goof";
        }
    }

    @GetMapping("/testdb")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public String home() {
        log.info("test database using controller");
        Conference testConference = new Conference();
        testConference.setNama("Cloud");
        testConference.setAbstrak("Computing");
        testConference.setRuanganDipakai("123");
        testConference.setTanggalMulai(new Date(0));

        log.info("set values for newly created conference " + testConference);
        if (conferenceRepo.findByNama("Cloud").size() > 0) {
            log.info("conference found, database working");
            return "Yep, there's already one in the database. It's working.";
        } else {
            conferenceRepo.save(testConference);
            log.info("conference not found, now saved in database");
        }

        log.info("test successful");
        return "If you're seeing this, then most likely the test is finished smoothly";
    }

    @GetMapping("/goof")
    @PreAuthorize("isAuthenticated()")
    public String customErrorPage(Model model) {
        log.warning("display error page");
        return "error/custom_error";
    }
}