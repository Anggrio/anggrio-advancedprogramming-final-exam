package com.adf.tugasakhir.controller;

import java.security.Principal;

import lombok.extern.java.Log;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Log
@Controller
public class AuthController {

  @GetMapping("/login")
  @PreAuthorize("permitAll()")
  public String login(final Principal principal) {
    log.info("display login page");
    if (principal != null) {
      log.info("user is logged in");
      return "redirect:/";
    }
    log.info("user is not logged in");
    return "_login.html";
  }
}
