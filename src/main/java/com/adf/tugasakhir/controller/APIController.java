package com.adf.tugasakhir.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.adf.tugasakhir.dataclass.Document;
import com.adf.tugasakhir.dataclass.Documents;
import com.adf.tugasakhir.dataclass.KeyPhrasesResult;
import com.adf.tugasakhir.service.ConferenceService;
import com.adf.tugasakhir.service.PaperFinderService;
import com.adf.tugasakhir.service.TextAnalysisService;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * CheckController
 * This controller's main objective is to make sure
 * that the service is actually usable and not 
 */
@Log
@RestController
@RequestMapping("api")
public class APIController {

    @Autowired
    TextAnalysisService textAnalysisService;

    @Autowired
    ConferenceService conferenceService;

    @Autowired
    PaperFinderService paperFinderService;

    @GetMapping()
    @PreAuthorize("permitAll()")
    @ResponseBody
    public Map<String, Serializable> information() {
        log.info("service for information api");
        Map<String, Serializable> information = new HashMap<>();
        List<String> resourceList = new ArrayList<>();
        
        // TODO add the resource information here
        resourceList.add(conferenceService.getAllLatestConference().toString());
        resourceList.add(paperFinderService.getAllPaper().toString());
        information.put("resources", (Serializable) resourceList);
        log.info("map containing information " + information);
        return information;
    }

    @GetMapping("test-text-analysis")
    @PreAuthorize("permitAll()")
    @ResponseBody
    public KeyPhrasesResult testTextAnalysis(@RequestParam(name = "language", required = true) String language,
            @RequestParam(name = "text", required = true) String text) {
        log.info("service for test text analysis api");
        Documents docs = new Documents();
        docs.addDocument(new Document("", language, text));
        return textAnalysisService.extractKeyPhrases(docs);
    }
}