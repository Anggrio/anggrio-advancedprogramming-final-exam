package com.adf.tugasakhir.dataclass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * KeyPhraseResponseDocument
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter @NoArgsConstructor
public class KeyPhrasesResponseDocument {

    @JsonProperty("id")
    String id;

    @JsonProperty("keyPhrases")
    List<String> keyPhrases = Collections.synchronizedList(new ArrayList<String>());

    public KeyPhrasesResponseDocument(String id, List<String> keyPhrases) {
        this.id = id;
        this.keyPhrases = keyPhrases;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof KeyPhrasesResponseDocument)) {
            return false;
        }
        KeyPhrasesResponseDocument keyPhraseResponseDocument = (KeyPhrasesResponseDocument) o;
        return Objects.equals(id, keyPhraseResponseDocument.id) && Objects.equals(keyPhrases, keyPhraseResponseDocument.keyPhrases);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, keyPhrases);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", keyPhrases='" + getKeyPhrases() + "'" +
            "}";
    }
}