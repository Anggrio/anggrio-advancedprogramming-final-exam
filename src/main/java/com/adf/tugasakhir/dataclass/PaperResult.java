package com.adf.tugasakhir.dataclass;

import com.adf.tugasakhir.model.Paper;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class PaperResult {

    private List<Paper> paperList = new ArrayList<>();
    private boolean isError;

    public boolean isError() {
        return isError;
    }
}
