package com.adf.tugasakhir.dataclass;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Document
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter @NoArgsConstructor
public class Document {

    @JsonProperty("id")
    String id;

    @JsonProperty("language")
    String language;

    @JsonProperty("text")
    String text;

    public Document(String id, String language, String text) {
        this.id = id;
        this.language = language;
        this.text = text;
    }

    public Document id(String id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Document)) {
            return false;
        }
        Document document = (Document) o;
        return Objects.equals(id, document.id) && Objects.equals(language, document.language) && Objects.equals(text, document.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, language, text);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", language='" + getLanguage() + "'" +
            ", text='" + getText() + "'" +
            "}";
    }
}
