package com.adf.tugasakhir.dataclass;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Documents
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter @NoArgsConstructor
public class Documents {

    @JsonProperty("documents")
    List<Document> documents = new ArrayList<>();    

    public Documents(List<Document> documents) {
        this.documents = documents;
    }

    public void addDocument(Document document) {
        document.setId(String.valueOf(documents.size() + 1));
        documents.add(document);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Documents)) {
            return false;
        }
        Documents documents = (Documents) o;
        return Objects.equals(documents, documents);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(documents);
    }

    @Override
    public String toString() {
        return "{" +
            " documents='" + getDocuments() + "'" +
            "}";
    }
}