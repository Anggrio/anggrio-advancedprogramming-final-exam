package com.adf.tugasakhir.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "paper")
@Getter @Setter @NoArgsConstructor
public class Paper {

    @Id
    private String title;

    private String abstrak;
    private String url;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Paper{" + "title='" + title + '\'' + ", abstrak='" + abstrak + '\'' + ", url='" + url + '\'' + '}';
    }
}
