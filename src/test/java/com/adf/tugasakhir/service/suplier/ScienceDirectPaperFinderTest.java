package com.adf.tugasakhir.service.suplier;

import com.adf.tugasakhir.model.Paper;
import com.adf.tugasakhir.dataclass.PaperResult;
import com.adf.tugasakhir.service.supplier.ScienceDirectPaperFinder;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ScienceDirectPaperFinderTest {

    @Test
    public void getInstanceCanReturnScienceDirectInstance() {
        ScienceDirectPaperFinder scienceDirectPaperFinder = ScienceDirectPaperFinder.getInstance();
        assertNotNull(scienceDirectPaperFinder);
    }

    @Test
    public void scienceDirectPageCanReturnPaperWithValidInput() {
        ScienceDirectPaperFinder scienceDirectPaperFinder = ScienceDirectPaperFinder.getInstance();
        PaperResult allpapers = scienceDirectPaperFinder.findPapers("Cloud computing");
        assertNotNull(allpapers);
        assertNotNull(allpapers.getPaperList());
        //assertFalse(allpapers.isError());

        List<Paper> paperRecieved = allpapers.getPaperList();
        //assertTrue(paperRecieved.size() > 0);
    }

    @Test
    public void scienceDirectPageCanReturnResponseWithInvalidInput() {
        ScienceDirectPaperFinder scienceDirectPaperFinder = ScienceDirectPaperFinder.getInstance();
        PaperResult allpapers = scienceDirectPaperFinder.findPapers("ancol");
        assertNotNull(allpapers);
        assertNotNull(allpapers.getPaperList());
        assertTrue(allpapers.isError());

        List<Paper> paperRecieved = allpapers.getPaperList();
        assertEquals(0, paperRecieved.size());
    }

}