# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Anggrio Wildanhadi Sutopo 1806173544

### Task 1: Ensuring 12 Factors (mandatory)
##### 1. Codebase: 
No issues, since there is only one branch (master). However, to simulate the capability, the "original" branch is created that stores the initial pull from the given gitlab project. Now, both master and original branches has the same codebase, but master will be ahead in terms of commits.
##### 2. Dependencies:
No issues, since dependencies are explicitly stated in the build.gradle file, and are built/downloaded by gradle. However, some dependencies are added based on the other tasks, such as spring boot actuator, lombok, and prometheus. The additional dependencies are explicitly written in the dependencies section in build.gradle, with some additional details on when it is used such as compile or implementation.
##### 3. Config:
Some parts are initially explicitly stated, such as server port and text.analytics.api.key. The solution is to change them to use environment variables SERVER_PORT and ANALYTICS_API_KEY, respectively. The environment variables are set in the local IDE (Intellij IDEA), gitlab repository, and the heroku application.
##### 4. Backing services:
No issues, since it is also part of the dependencies that is handled by build.gradle. The heroku postgre addon is added for the heroku app
##### 5. Build, release, run:
No issues, the given gitlab-ci file already has a separate job for build and for deploy.
##### 6. Port binding:
No issues, the given application.properties already has a custom port, 8080. Changed to use environment variables as per Config factor requirement.
##### 7. Dev/prod parity:
Since the gitlab repository and heroku are newly created, they each have to be given the environment variables separately. Gitlab uses settings>CI/CD>variables to store the environment variables, while Heroku uses settings>config vars. Gitlab has the HEROKU_API_KEY and HEROKU_APP_NAME variables since it is required to deploy to the heroku, while heroku doesn't need to have it. Both have the variables JDBC_DATABASE_URL, JDBC_DATABASE_USERNAME, JDBC_DATABASE_PASSWORD SERVER_PORT, and ANALYTICS_API_KEY.
##### 8. Logs:
Initially have none, but using the @Logger annotation from Lombok as well as the Log.info method, each controller is given a logger in each of their methods.
##### 9. Admin processes: 
No issues, it is implemented by Spring boot by default
